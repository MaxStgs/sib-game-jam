using System;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public bool IsStart { get; set; } = true;

    private Text text;
    private double time;

    void Start()
    {
        text = GetComponent<Text>();
    }
    
    void Update()
    {
        if (IsStart)
        {
            time += Time.deltaTime;
            text.text = Math.Round(time, 2).ToString("0.00");
        }
    }
}
