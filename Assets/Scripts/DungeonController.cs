using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonController : EventScript
{
    private Collider2D _selfCollider;
    
    // Start is called before the first frame update
    void Start()
    {
        _selfCollider = GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void CallEvent(GameObject o, Collider2D other)
    {
        var controller = other.gameObject.GetComponentInParent<PlayerControllerScript>();
        if (!controller) return;

        controller.SliceBody(_selfCollider, other);
    }
}
