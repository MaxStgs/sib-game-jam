using System.Collections;
using System.ComponentModel.Design;
using UnityEditor.SearchService;
using UnityEngine;
using Object = UnityEngine.Object;

public class PlayerControllerScript : MonoBehaviour
{
    public float forceAmount = 500;
    public Rigidbody2D selectedRigidbody;
    public Camera targetCamera;
    public Object levelFinishedUI;
    public Object gameOverUI;
    public AudioClip[] screamSounds;
    public AudioClip[] startMoveSounds;
    public AudioClip[] endMoveSounds;
    public AudioClip[] moveSounds;
    public AudioSource screamAudioSource;

    private bool _isControlEnabled = false;
    private float _selectionDistance;
    private Vector3 _originalScreenTargetPosition;
    private Vector3 _originalRigidbodyPos;
    private bool _isAlive = true;

    private Timer _timer;

    // Start is called before the first frame update
    void Start()
    {
        _isControlEnabled = true;
        _timer = FindObjectOfType<Timer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            #region Звук взятия и переноса персонажа

            StartCoroutine(PlaySound(startMoveSounds));
            StartCoroutine(PlaySound(moveSounds));            

            #endregion
            
            //Check if we are hovering over Rigidbody, if so, select it
            selectedRigidbody = GetRigidbodyFromMouseClick();
        }

        if (Input.GetMouseButtonUp(0) && selectedRigidbody)
        {
            //Release selected Rigidbody if there any
            selectedRigidbody = null;

            #region Остановка звука переноса персонажа и звук отпускания персонажа

            screamAudioSource.Stop();
            StartCoroutine(PlaySound(endMoveSounds));            

            #endregion
        }
    }

    private IEnumerator PlaySound(AudioClip[] clips)
    {
        screamAudioSource.PlayOneShot(clips[Random.Range(0, clips.Length)]);
        yield return null;
    }

    private void FixedUpdate()
    {
        if (selectedRigidbody && _isControlEnabled)
        {
            var pos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, _selectionDistance);
            var mousePositionOffset = targetCamera.ScreenToWorldPoint(pos) - _originalScreenTargetPosition;
            selectedRigidbody.velocity =
                (_originalRigidbodyPos + mousePositionOffset - selectedRigidbody.transform.position) * forceAmount *
                Time.deltaTime;
        }
    }

    Rigidbody2D GetRigidbodyFromMouseClick()
    {
        var ray = targetCamera.ScreenPointToRay(Input.mousePosition);
        Debug.DrawRay(ray.origin, ray.direction * 10, Color.red);
        Debug.DrawLine(ray.origin, ray.direction * 10, Color.green);

        var hit = Physics2D.Raycast(ray.origin, ray.direction);

        if (!hit) return null;

        if (!hit.collider.gameObject.GetComponent<Rigidbody2D>()) return null;

        _selectionDistance = Vector3.Distance(ray.origin, hit.point);
        _originalScreenTargetPosition =
            targetCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y,
                _selectionDistance));
        _originalRigidbodyPos = hit.collider.transform.position;
        return hit.collider.gameObject.GetComponent<Rigidbody2D>();
    }
    
    public void SliceBody(Collider2D damager, Collider2D bodyPart)
    {
        SliceBodyInternal(bodyPart);
    }

    private void SliceBodyInternal(Collider2D bodyPart)
    {
        if (!bodyPart.gameObject) return;

        if (bodyPart.name == "Head" || bodyPart.name == "Torso")
        {
            if (!_isAlive) return;
            
            _isControlEnabled = false;
            GameOver();
            return;
        }

        var hingeJoint2D = bodyPart.gameObject.GetComponent<HingeJoint2D>();
        if (!screamAudioSource.isPlaying)
        {
            screamAudioSource.PlayOneShot(screamSounds[Random.Range(0, screamSounds.Length)]);
        }

        if (!hingeJoint2D) return;

        hingeJoint2D.breakForce = 0;
        // Destroy(bodyPart.gameObject);
    }

    public int GetCountBodies()
    {
        var count = 1;
        for (var i = 0; i < gameObject.transform.childCount; i++)
        {
            var child = gameObject.transform.GetChild(i);
            if (!child.CompareTag("PlayerBody"))
            {
                continue;
            }

            var hingeJoint2D = child.GetComponent<HingeJoint2D>();
            if (!hingeJoint2D || hingeJoint2D.breakForce == 0) continue;

            count++;
        }

        return count;
    }

    public LevelFinishedUIController SpawnLevelFinishedUI()
    {
        if (!levelFinishedUI)
        {
            print("Can not spawn UI, not found");
            return null;
        }

        var ui = Instantiate(levelFinishedUI);
        if (!ui)
        {
            print("Can not throw Instantiate UI");
            return null;
        }

        var o = (GameObject)ui;
        var controller = o.GetComponent<LevelFinishedUIController>();
        if (!controller)
        {
            print("Level Finished UI Controller not found");
            return null;
        }

        return controller;
    }

    private void GameOver()
    {
        // Останавливаем таймер
        _timer.IsStart = false;
        _isAlive = false;
        var screen = SpawnGameOverUI();
    }

    public void SetupControl(bool newControlEnabled)
    {
        _isControlEnabled = newControlEnabled;
    }

    private GameOverUIController SpawnGameOverUI()
    {
        if (!gameOverUI)
        {
            print("Can not spawn UI, not found");
            return null;
        }

        var ui = Instantiate(gameOverUI);
        if (!ui)
        {
            print("Can not throw Instantiate UI");
            return null;
        }

        var o = (GameObject)ui;
        var controller = o.GetComponent<GameOverUIController>();
        if (!controller)
        {
            print("Can not find controller");
            return null;
        }

        return controller;
    }
}