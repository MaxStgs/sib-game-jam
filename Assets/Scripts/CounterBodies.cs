using UnityEngine;
using UnityEngine.UI;

public class CounterBodies : MonoBehaviour
{
    private PlayerControllerScript _playerController;
    private Text _text;
    void Start()
    {
        _playerController = FindObjectOfType<PlayerControllerScript>();
        _text = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        _text.text = $"{_playerController.GetCountBodies().ToString()}/10";
    }
}
