using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDelegateScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    private void OnCollisionEnter2D(Collision2D other)
    {
        var eventScript = GetComponentInParent<EventScript>();
        if (!eventScript) return;

        eventScript.CallEvent(gameObject, other.collider);
    }
}
