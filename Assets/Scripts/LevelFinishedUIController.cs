using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelFinishedUIController : MonoBehaviour
{
    private string _nextLevelName;

    public Button continueButton;
    public Text text;
    
    // Start is called before the first frame update
    void Start()
    {
        if (!continueButton) return;
        
        continueButton.onClick.AddListener(OnClick);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnClick()
    {
        if (_nextLevelName == "")
        {
            print("No next level, open Level1");
            SceneManager.LoadScene("Level1");
        }
        else
        {
            print($"Open next level with name");
            SceneManager.LoadScene(_nextLevelName);
        }
    }

    public void Setup(string happinessText, string nextLevelName, bool isGameOver = false)
    {
        _nextLevelName = nextLevelName;
        text.text = happinessText;

        if (isGameOver)
        {
            var textComponent = continueButton.transform.GetChild(0).GetComponent<Text>();
            textComponent.text = "Retry";
        }
    }
}
