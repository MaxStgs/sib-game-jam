using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverUIController : MonoBehaviour
{
    public Button ButtonContinue;
    
    // Start is called before the first frame update
    void Start()
    {
        if (ButtonContinue)
        {
            ButtonContinue.onClick.AddListener(OnClick);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnClick()
    {
        var currentSceneName = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene(currentSceneName);
    }
}
