using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelFinishController : MonoBehaviour
{
    private string _happinessText;
    private string _nextLevelName;

    private bool _isLevelFinished = false;

    // Method for setup parameters, Execute should be called manually
    public void Setup(string happinessText, string nextLevelName)
    {
        _happinessText = happinessText;
        _nextLevelName = nextLevelName;
    }

    // Called after Setup
    public void Execute()
    {
        if (_isLevelFinished) return;
        
        var stickman = GameObject.Find("Stickman");
        if (!stickman)
        {
            throw new Exception("Stickman does not found");
        }

        var playerController = stickman.GetComponent<PlayerControllerScript>();
        if (!playerController)
        {
            throw new Exception("Stickman Player Controller does not found");
        }

        var count = playerController.GetCountBodies();
        var finishedUI = playerController.SpawnLevelFinishedUI();
        finishedUI.Setup(_happinessText, _nextLevelName);
        print($"Level finished with {count} bodies");
        playerController.SetupControl(false);
        _isLevelFinished = true;
    }
}
