using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitScript : EventScript
{
    public string nextLevelName;
    public string happinessText = "Hey, Congratulations";

    private bool _isExitActivated = false;

    private Timer _timer;

    // Start is called before the first frame update
    void Start()
    {
        _timer = FindObjectOfType<Timer>();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public override void CallEvent(GameObject o, Collider2D other)
    {
        if (!other.gameObject || _isExitActivated || other.gameObject.name != "Head") return;

        // Останавливаем таймер
        _timer.IsStart = false;
        
        _isExitActivated = true;
        print("Exit activated");
        var finishController = gameObject.AddComponent<LevelFinishController>();
        finishController.Setup(happinessText, nextLevelName);
        finishController.Execute();
    }
}